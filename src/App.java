public class App {
    public static void main(String[] args) throws Exception {
        //khai báo đối tượng hình chữ nhật
        Rectangle rectangle = new Rectangle(4,3);
        Triangle triangle = new Triangle(4, 3);
        System.out.println("Rectangle");
        System.out.println(rectangle);
        System.out.println("Triangle");
        System.out.println(triangle);

        //tính diện tích
        System.out.println("Area of Rectangle");
        System.out.println(rectangle.getArea());
        System.out.println("Area of Triangle");
        System.out.println(triangle.getArea());


    }
}
