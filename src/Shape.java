public abstract class Shape {
    private String color;

    public Shape() {
    }
    
    public abstract double getArea();
    
}
