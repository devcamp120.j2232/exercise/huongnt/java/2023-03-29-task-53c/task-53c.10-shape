public class Triangle extends Shape {
    private double base;
    private double height;
    

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }


    @Override
    public String toString() {
        return "Triangle [base=" + base + ", height=" + height + "]";
    }


    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return (base*height)/2;
    }

    
    
}
